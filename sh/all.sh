#! /bin/bash

./build.sh

if [ ! $? -eq 0 ]
then
	exit 1
fi

echo "--- Run ---"
echo
./run.sh

echo