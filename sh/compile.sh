#! /bin/bash

for file in `find "../src" -name '*.cpp'`
do
	echo "Compiling $file"

	dir=`dirname $file | sed -E 's/..\/src\/?//'`
	mkdir -p "../obj/$dir"

	path=`echo $file | sed 's/..\/src\///' | sed 's/.cpp//'`
	g++ -std=c++17  -iquote "../src" -D debug -c "../src/$path.cpp" -o "../obj/$path.o"

	if [ ! $? -eq 0 ]
	then
		exit 1
	fi

done
