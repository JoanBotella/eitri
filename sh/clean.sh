#! /bin/bash

if [ -d "../obj" ]
then
	rm -rf "../obj"

	if [ ! $? -eq 0 ]
	then
		exit 1
	fi
fi

mkdir "../obj"

if [ ! $? -eq 0 ]
then
	exit 1
fi

if [ -d "../bin" ]
then
	rm -rf "../bin"

	if [ ! $? -eq 0 ]
	then
		exit 1
	fi
fi

mkdir "../bin"

if [ ! $? -eq 0 ]
then
	exit 1
fi
