#! /bin/bash

mkdir -p "../bin/eitri/demo" 2> /dev/null

g++ -o "../bin/eitri/demo/main" `find "../obj" -name "*.o"`

if [ ! $? -eq 0 ]
then
	exit 1
fi
