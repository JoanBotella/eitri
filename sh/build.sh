#! /bin/bash

echo
echo "--- Clean ---"
echo
./clean.sh

if [ ! $? -eq 0 ]
then
	exit 1
fi

echo "--- Compile ---"
echo
./compile.sh

if [ ! $? -eq 0 ]
then
	exit 1
fi

echo

echo "--- Link ---"
echo
./link.sh

if [ ! $? -eq 0 ]
then
	exit 1
fi
