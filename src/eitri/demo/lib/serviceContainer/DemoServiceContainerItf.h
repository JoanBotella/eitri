#pragma once

#include "eitri/fw/lib/serviceContainer/FwServiceContainerItf.h"

using eitri::fw::lib::serviceContainer::FwServiceContainerItf;

namespace eitri::demo::lib::serviceContainer
{

	class DemoServiceContainerItf
	:
		virtual public FwServiceContainerItf
	{

		public:

			virtual ~DemoServiceContainerItf() { };

	};

}