#include "eitri/demo/lib/serviceContainer/DemoServiceContainer.h"
#include <iostream>

using eitri::demo::lib::serviceContainer::DemoServiceContainer;
using std::cout;
using std::endl;

namespace eitri::demo::lib::serviceContainer
{

	DemoServiceContainer::DemoServiceContainer()
	{
		#ifdef debug
		cout << "\033[0;32mConstructing DemoServiceContainer\033[0m" << endl;
		#endif
	}

	DemoServiceContainer::DemoServiceContainer(const DemoServiceContainer& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing DemoServiceContainer by copy\033[0m" << endl;
		#endif
	}

	DemoServiceContainer::~DemoServiceContainer()
	{
		#ifdef debug
		cout << "\033[0;31mDestroying DemoServiceContainer\033[0m" << endl;
		#endif
	}

}