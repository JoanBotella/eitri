#pragma once

#include "eitri/demo/lib/serviceContainer/DemoServiceContainerItf.h"
#include "eitri/fw/lib/serviceContainer/FwServiceContainerAbs.h"

using eitri::demo::lib::serviceContainer::DemoServiceContainerItf;
using eitri::fw::lib::serviceContainer::FwServiceContainerAbs;

namespace eitri::demo::lib::serviceContainer
{

	class DemoServiceContainer
	:
		virtual public DemoServiceContainerItf,
		public FwServiceContainerAbs
	{

		public:

			DemoServiceContainer();

			DemoServiceContainer(const DemoServiceContainer& src);

			~DemoServiceContainer();

	};

}