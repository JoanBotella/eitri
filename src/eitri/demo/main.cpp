#include "eitri/demo/lib/serviceContainer/DemoServiceContainer.h"
#include "eitri/demo/lib/serviceContainer/DemoServiceContainerItf.h"
#include "eitri/fw/lib/error/FwError.h"
#include "eitri/fw/lib/request/FwRequest.h"
#include "eitri/fw/lib/response/FwResponse.h"
#include "eitri/fw/lib/service/FwServiceOutput.h"
#include "eitri/fw/service/cgiApp/FwCgiApp.h"
#include "eitri/fw/service/cgiApp/FwCgiAppInput.h"
#include "eitri/fw/service/cgiApp/FwCgiAppItf.h"
#include "eitri/fw/service/cgiApp/FwCgiAppResult.h"
#include <iostream>
#include <string>
#include <vector>

using eitri::demo::lib::serviceContainer::DemoServiceContainer;
using eitri::demo::lib::serviceContainer::DemoServiceContainerItf;
using eitri::fw::lib::error::FwError;
using eitri::fw::lib::request::FwRequest;
using eitri::fw::lib::response::FwResponse;
using eitri::fw::lib::service::FwServiceOutput;
using eitri::fw::service::cgiApp::FwCgiApp;
using eitri::fw::service::cgiApp::FwCgiAppInput;
using eitri::fw::service::cgiApp::FwCgiAppItf;
using eitri::fw::service::cgiApp::FwCgiAppResult;
using std::cout;
using std::endl;
using std::string;
using std::vector;

int main()
{
	DemoServiceContainerItf* serviceContainer = new DemoServiceContainer();

	FwCgiAppItf* cgiApp = serviceContainer->getCgiApp();

	string* path = new string("/");
	FwRequest* request = new FwRequest(path);
	FwCgiAppInput* input = new FwCgiAppInput(request);

	FwServiceOutput<FwCgiAppResult>* cgiAppOutput = cgiApp->run(input);

	if (cgiAppOutput->hasErrors())
	{
		vector<FwError*>* errs = cgiAppOutput->getErrorsAfterHas();
		for (int i = 0; i < errs->size(); i++)
		{
			FwError* err = errs->at(i);
			cout << "\033[48;5;9m" << err->getMessage() << "\033[0m" << endl;
			delete err;
		}
		errs->clear();

		delete cgiAppOutput;

		return 1;
	}

	if (!cgiAppOutput->hasResult())
	{
		cout << "\033[48;5;9mThe cgiApp has no errors nor result\033[0m" << endl;

		delete cgiAppOutput;

		return 1;
	}

	FwCgiAppResult* result = cgiAppOutput->getResultAfterHas();
	FwResponse* response = result->getResponse();
	string* body = response->getBody();

	cout << endl;
	cout << *body << endl;
	cout << endl;

	delete body;
	delete response;
	delete result;
	delete cgiAppOutput;

	delete serviceContainer;

	return 0;
}