#include "eitri/fw/lib/request/FwRequest.h"
#include <iostream>
#include <string>

using eitri::fw::lib::request::FwRequest;
using std::cout;
using std::endl;
using std::string;

namespace eitri::fw::lib::request
{

	FwRequest::FwRequest(string* path)
	:
		path(path)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwRequest\033[0m" << endl;
		#endif
	}

	FwRequest::FwRequest(const FwRequest& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwRequest by copy\033[0m" << endl;
		#endif
	}

	FwRequest::~FwRequest()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwRequest\033[0m" << endl;
		#endif
	}

	string* FwRequest::getPath()
	{
		return this->path;
	}

}