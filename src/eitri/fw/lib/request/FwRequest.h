#pragma once

#include <string>

using std::string;

namespace eitri::fw::lib::request
{

	class FwRequest
	{

		public:

			FwRequest(string* path);

			FwRequest(const FwRequest& src);

			~FwRequest();

			string* getPath();

		private:

			string* path;

	};

}