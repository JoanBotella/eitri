#include "eitri/fw/lib/error/FwError.h"
#include <iostream>
#include <string>

using eitri::fw::lib::error::FwError;
using std::cout;
using std::endl;
using std::string;

namespace eitri::fw::lib::error
{

	FwError::FwError(string* message)
	:
		message(message)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwError\033[0m" << endl;
		#endif
	}

	FwError::FwError(const FwError& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwError by copy\033[0m" << endl;
		#endif
	}

	FwError::~FwError()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwError\033[0m" << endl;
		#endif
	}

	string* FwError::getMessage()
	{
		return this->message;
	}

}