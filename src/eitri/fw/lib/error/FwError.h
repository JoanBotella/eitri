#pragma once

#include <string>

using std::string;

namespace eitri::fw::lib::error
{

	class FwError
	{

		public:

			FwError(string* message);

			FwError(const FwError& src);

			~FwError();

			string* getMessage();

		private:

			string* message;

	};

}