#pragma once

#include "eitri/fw/lib/service/FwServiceOutput.h"
#include <iostream>

using std::cout;
using std::endl;
using eitri::fw::lib::service::FwServiceOutput;

namespace eitri::fw::lib::service
{

	template<
		typename TInput,
		typename TResult
	>
	class FwServiceItf
	{

		public:

			virtual ~FwServiceItf() { }

			virtual FwServiceOutput<TResult>* run(TInput* input) = 0;

	};

}