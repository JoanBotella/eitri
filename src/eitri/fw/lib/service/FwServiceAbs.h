#pragma once

#include "eitri/fw/lib/service/FwServiceItf.h"
#include "eitri/fw/lib/service/FwServiceOutput.h"
#include <iostream>

using eitri::fw::lib::service::FwServiceItf;
using eitri::fw::lib::service::FwServiceOutput;
using std::cout;
using std::endl;

namespace eitri::fw::lib::service
{

	template<
		typename TInput,
		typename TResult
	>
	class FwServiceAbs
	:
		virtual public FwServiceItf<
			TInput,
			TResult
		>
	{

		public:

			FwServiceAbs()
			:
				input(NULL),
				output(NULL)
			{
				#ifdef debug
				cout << "\033[0;32m\tConstructing FwServiceAbs\033[0m" << endl;
				#endif
			}

			FwServiceAbs(const FwServiceAbs<TInput, TResult>& src)
			{
				#ifdef debug
				cout << "\033[0;33m\tConstructing FwServiceAbs by copy\033[0m" << endl;
				#endif
			}

			~FwServiceAbs()
			{
				#ifdef debug
				cout << "\033[0;31m\tDestructing FwServiceAbs\033[0m" << endl;
				#endif
			}

			FwServiceOutput<TResult>* run(TInput* input) override
			{
				this->input = input;
				this->output = new FwServiceOutput<TResult>();

				runService();

				deleteInput();

				return this->output;
			}

		protected:

			TInput* getInput() const
			{
				return this->input;
			}

			FwServiceOutput<TResult>* getOutput() const
			{
				return this->output;
			}

			void setOutput(FwServiceOutput<TResult>* output)
			{
				this->output = output;
			}

			virtual void runService() = 0;

			virtual void deleteInput() = 0;

		private:

			TInput* input;
			FwServiceOutput<TResult>* output;

	};

}