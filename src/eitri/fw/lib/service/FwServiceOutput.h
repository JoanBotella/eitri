#pragma once

#include "eitri/fw/lib/error/FwError.h"
#include <iostream>
#include <vector>

using eitri::fw::lib::error::FwError;
using std::cout;
using std::endl;
using std::vector;

namespace eitri::fw::lib::service
{

	template<
		typename TResult
	>
	class FwServiceOutput
	{

		public:

			FwServiceOutput()
			:
				errors(NULL),
				result(NULL)
			{
				#ifdef debug
				cout << "\033[0;32mConstructing FwServiceOutput\033[0m" << endl;
				#endif
			}

			FwServiceOutput(const FwServiceOutput<TResult>& src)
			{
				#ifdef debug
				cout << "\033[0;33mConstructing FwServiceOutput by copy\033[0m" << endl;
				#endif
			}

			~FwServiceOutput()
			{
				#ifdef debug
				cout << "\033[0;31mDestructing FwServiceOutput\033[0m" << endl;
				#endif
			}

			bool hasErrors()
			{
				return this->errors != NULL;
			}

			vector<FwError*>* getErrorsAfterHas()
			{
				return this->errors;
			}

			void addError(FwError* err)
			{
				vector<FwError*>* errs = new vector<FwError*>();
				errs->push_back(err);
				this->addErrors(errs);
			}

			void addErrors(vector<FwError*>* errs)
			{
				if (this->errors == NULL)
				{
					this->errors = errs;
					return;
				}

				for (int i = 0; i < errs->size(); i++)
				{
					FwError* err = errs->at(i);
					this->errors->push_back(err);
				}

				delete errs;
			}

			void emptyErrors()
			{
				if (this->errors == NULL)
				{
					return;
				}

				for (FwError* err : this->errors)
				{
					delete err;
				}
				this->errors->clear();

				this->errors = NULL;
			}

			bool hasResult()
			{
				return this->result != NULL;
			}

			TResult* getResultAfterHas()
			{
				return this->result;
			}

			void setResult(TResult* result)
			{
				this->result = result;
			}

			void unsetResult()
			{
				this->result = NULL;
			}

		private:

			vector<FwError*>* errors;
			TResult* result;

	};

}