#pragma once

#include <string>

using std::string;

namespace eitri::fw::lib::response
{

	class FwResponse
	{

		public:

			FwResponse(string* body);

			FwResponse(const FwResponse& src);

			~FwResponse();

			string* getBody();

		private:

			string* body;

	};

}