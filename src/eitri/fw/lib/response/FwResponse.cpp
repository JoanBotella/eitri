#include "eitri/fw/lib/response/FwResponse.h"
#include <iostream>
#include <string>

using eitri::fw::lib::response::FwResponse;
using std::cout;
using std::endl;
using std::string;

namespace eitri::fw::lib::response
{

	FwResponse::FwResponse(string* body)
	:
		body(body)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwResponse\033[0m" << endl;
		#endif
	}

	FwResponse::FwResponse(const FwResponse& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwResponse by copy\033[0m" << endl;
		#endif
	}

	FwResponse::~FwResponse()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwResponse\033[0m" << endl;
		#endif
	}

	string* FwResponse::getBody()
	{
		return this->body;
	}

}