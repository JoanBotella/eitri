#pragma once

#include "eitri/fw/lib/serviceContainer/FwServiceContainerItf.h"
#include "eitri/fw/service/cgiApp/FwCgiAppItf.h"
#include "eitri/fw/service/router/FwRouterItf.h"

using eitri::fw::lib::serviceContainer::FwServiceContainerItf;
using eitri::fw::service::cgiApp::FwCgiAppItf;
using eitri::fw::service::router::FwRouterItf;

namespace eitri::fw::lib::serviceContainer
{

	class FwServiceContainerAbs
	:
		virtual public FwServiceContainerItf
	{

		public:

			FwServiceContainerAbs();

			FwServiceContainerAbs(const FwServiceContainerAbs& src);

			virtual ~FwServiceContainerAbs();

			virtual FwCgiAppItf* getCgiApp() const override;

			virtual FwRouterItf* getRouter() const override;

		private:

			mutable FwCgiAppItf* cgiApp;

			FwCgiAppItf* buildCgiApp() const;

			mutable FwRouterItf* router;

			FwRouterItf* buildRouter() const;

	};

}