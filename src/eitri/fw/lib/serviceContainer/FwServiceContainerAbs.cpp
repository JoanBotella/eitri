#include "eitri/fw/lib/serviceContainer/FwServiceContainerAbs.h"
#include "eitri/fw/service/cgiApp/FwCgiApp.h"
#include "eitri/fw/service/cgiApp/FwCgiAppItf.h"
#include "eitri/fw/service/router/FwRouter.h"
#include "eitri/fw/service/router/FwRouterItf.h"
#include <iostream>

using eitri::fw::service::cgiApp::FwCgiApp;
using eitri::fw::service::cgiApp::FwCgiAppItf;
using eitri::fw::service::router::FwRouter;
using eitri::fw::service::router::FwRouterItf;
using std::cout;
using std::endl;

namespace eitri::fw::lib::serviceContainer
{

	FwServiceContainerAbs::FwServiceContainerAbs()
	:
		cgiApp(NULL)
	{
		#ifdef debug
		cout << "\033[0;32m\tConstructing FwServiceContainerAbs\033[0m" << endl;
		#endif
	}

	FwServiceContainerAbs::FwServiceContainerAbs(const FwServiceContainerAbs& src)
	{
		#ifdef debug
		cout << "\033[0;33m\tConstructing FwServiceContainerAbs by copy\033[0m" << endl;
		#endif
	}

	FwServiceContainerAbs::~FwServiceContainerAbs()
	{
		#ifdef debug
		cout << "\033[0;31m\tDestroying FwServiceContainerAbs\033[0m" << endl;
		#endif

		delete this->cgiApp;
		delete this->router;
	}

	FwCgiAppItf* FwServiceContainerAbs::getCgiApp() const
	{
		if (this->cgiApp == NULL)
		{
			this->cgiApp = buildCgiApp();
		}
		return this->cgiApp;
	}

		FwCgiAppItf* FwServiceContainerAbs::buildCgiApp() const
		{
			return new FwCgiApp(
				this->getRouter()
			);
		}

	FwRouterItf* FwServiceContainerAbs::getRouter() const
	{
		if (this->router == NULL)
		{
			this->router = buildRouter();
		}
		return this->router;
	}

		FwRouterItf* FwServiceContainerAbs::buildRouter() const
		{
			return new FwRouter(
			);
		}

}