#pragma once

#include "eitri/fw/service/cgiApp/FwCgiAppItf.h"
#include "eitri/fw/service/router/FwRouterItf.h"

using eitri::fw::service::cgiApp::FwCgiAppItf;
using eitri::fw::service::router::FwRouterItf;

namespace eitri::fw::lib::serviceContainer
{

	class FwServiceContainerItf
	{

		public:

			virtual ~FwServiceContainerItf() { };

			virtual FwCgiAppItf* getCgiApp() const = 0;

			virtual FwRouterItf* getRouter() const = 0;

	};

}