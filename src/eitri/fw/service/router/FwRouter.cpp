#include "eitri/fw/lib/response/FwResponse.h"
#include "eitri/fw/lib/service/FwServiceOutput.h"
#include "eitri/fw/service/router/FwRouter.h"
#include "eitri/fw/service/router/FwRouterInput.h"
#include "eitri/fw/service/router/FwRouterResult.h"
#include <iostream>
#include <string>

using eitri::fw::lib::response::FwResponse;
using eitri::fw::lib::service::FwServiceOutput;
using eitri::fw::service::router::FwRouter;
using eitri::fw::service::router::FwRouterInput;
using eitri::fw::service::router::FwRouterResult;
using std::cout;
using std::endl;
using std::string;

namespace eitri::fw::service::router
{

	FwRouter::FwRouter()
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwRouter\033[0m" << endl;
		#endif
	}

	FwRouter::FwRouter(const FwRouter& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwRouter by copy\033[0m" << endl;
		#endif
	}

	FwRouter::~FwRouter()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwRouter\033[0m" << endl;
		#endif
	}

	void FwRouter::runService()
	{
		cout << "FwRouter::runService" << endl;

		FwServiceOutput<FwRouterResult>* output = this->getOutput();
		string* body = new string("Hello World");
		FwResponse* response = new FwResponse(body);
		FwRouterResult* result = new FwRouterResult(response);
		output->setResult(result);
		this->setOutput(output);
	}

	void FwRouter::deleteInput()
	{
		FwRouterInput* input = this->getInput();
		FwRequest* request = input->getRequest();
		string* path = request->getPath();
		delete path;
		delete request;
		delete input;
	}

}