#pragma once

#include "eitri/fw/lib/request/FwRequest.h"

using eitri::fw::lib::request::FwRequest;

namespace eitri::fw::service::router
{

	class FwRouterInput
	{

		public:

			FwRouterInput(FwRequest* request);

			FwRouterInput(const FwRouterInput& src);

			~FwRouterInput();

			FwRequest* getRequest();

		private:

			FwRequest* request;

	};

}