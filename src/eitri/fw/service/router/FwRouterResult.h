#pragma once

#include "eitri/fw/lib/response/FwResponse.h"

using eitri::fw::lib::response::FwResponse;

namespace eitri::fw::service::router
{

	class FwRouterResult
	{

		public:

			FwRouterResult(FwResponse* response);

			FwRouterResult(const FwRouterResult& src);

			~FwRouterResult();

			FwResponse* getResponse();

		private:

			FwResponse* response;

	};

}