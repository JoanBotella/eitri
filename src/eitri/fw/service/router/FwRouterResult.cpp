#include "eitri/fw/lib/response/FwResponse.h"
#include "eitri/fw/service/router/FwRouterResult.h"
#include <iostream>

using eitri::fw::lib::response::FwResponse;
using eitri::fw::service::router::FwRouterResult;
using std::cout;
using std::endl;

namespace eitri::fw::service::router
{

	FwRouterResult::FwRouterResult(FwResponse* response)
	:
		response(response)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwRouterResult\033[0m" << endl;
		#endif
	}

	FwRouterResult::FwRouterResult(const FwRouterResult& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwRouterResult by copy\033[0m" << endl;
		#endif
	}

	FwRouterResult::~FwRouterResult()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwRouterResult\033[0m" << endl;
		#endif
	}

	FwResponse* FwRouterResult::getResponse()
	{
		return this->response;
	}

}