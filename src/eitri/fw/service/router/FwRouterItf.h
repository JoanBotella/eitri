#pragma once

#include "eitri/fw/lib/service/FwServiceItf.h"
#include "eitri/fw/service/router/FwRouterInput.h"
#include "eitri/fw/service/router/FwRouterResult.h"

using eitri::fw::lib::service::FwServiceItf;
using eitri::fw::service::router::FwRouterInput;
using eitri::fw::service::router::FwRouterResult;

namespace eitri::fw::service::router
{

	class FwRouterItf
	:
		virtual public FwServiceItf<
			FwRouterInput,
			FwRouterResult
		>
	{

		public:

			virtual ~FwRouterItf() { };

	};

}