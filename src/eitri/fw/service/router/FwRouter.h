#pragma once

#include "eitri/fw/lib/service/FwServiceAbs.h"
#include "eitri/fw/service/router/FwRouterInput.h"
#include "eitri/fw/service/router/FwRouterItf.h"
#include "eitri/fw/service/router/FwRouterResult.h"

using eitri::fw::lib::service::FwServiceAbs;
using eitri::fw::service::router::FwRouterInput;
using eitri::fw::service::router::FwRouterItf;
using eitri::fw::service::router::FwRouterResult;

namespace eitri::fw::service::router
{

	class FwRouter
	:
		virtual public FwRouterItf,
		public FwServiceAbs<
			FwRouterInput,
			FwRouterResult
		>
	{

		public:

			FwRouter();

			FwRouter(const FwRouter& src);

			~FwRouter();

		protected:

			void runService() override;

			void deleteInput() override;

	};

}