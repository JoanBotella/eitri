#include "eitri/fw/lib/request/FwRequest.h"
#include "eitri/fw/service/router/FwRouterInput.h"
#include <iostream>

using eitri::fw::lib::request::FwRequest;
using eitri::fw::service::router::FwRouterInput;
using std::cout;
using std::endl;

namespace eitri::fw::service::router
{

	FwRouterInput::FwRouterInput(FwRequest* request)
	:
		request(request)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwRouterInput\033[0m" << endl;
		#endif
	}

	FwRouterInput::FwRouterInput(const FwRouterInput& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwRouterInput by copy\033[0m" << endl;
		#endif
	}

	FwRouterInput::~FwRouterInput()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwRouterInput\033[0m" << endl;
		#endif
	}

	FwRequest* FwRouterInput::getRequest()
	{
		return this->request;
	}

}