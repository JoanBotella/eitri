#pragma once

#include "eitri/fw/lib/response/FwResponse.h"

using eitri::fw::lib::response::FwResponse;

namespace eitri::fw::service::cgiApp
{

	class FwCgiAppResult
	{

		public:

			FwCgiAppResult(FwResponse* response);

			FwCgiAppResult(const FwCgiAppResult& src);

			~FwCgiAppResult();

			FwResponse* getResponse();

		private:

			FwResponse* response;

	};

}