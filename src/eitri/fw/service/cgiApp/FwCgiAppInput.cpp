#include "eitri/fw/lib/request/FwRequest.h"
#include "eitri/fw/service/cgiApp/FwCgiAppInput.h"
#include <iostream>

using eitri::fw::lib::request::FwRequest;
using eitri::fw::service::cgiApp::FwCgiAppInput;
using std::cout;
using std::endl;

namespace eitri::fw::service::cgiApp
{

	FwCgiAppInput::FwCgiAppInput(FwRequest* request)
	:
		request(request)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwCgiAppInput\033[0m" << endl;
		#endif
	}

	FwCgiAppInput::FwCgiAppInput(const FwCgiAppInput& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwCgiAppInput by copy\033[0m" << endl;
		#endif
	}

	FwCgiAppInput::~FwCgiAppInput()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwCgiAppInput\033[0m" << endl;
		#endif
	}

	FwRequest* FwCgiAppInput::getRequest()
	{
		return this->request;
	}

}