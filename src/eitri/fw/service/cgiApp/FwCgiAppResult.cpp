#include "eitri/fw/lib/response/FwResponse.h"
#include "eitri/fw/service/cgiApp/FwCgiAppResult.h"
#include <iostream>

using eitri::fw::lib::response::FwResponse;
using eitri::fw::service::cgiApp::FwCgiAppResult;
using std::cout;
using std::endl;

namespace eitri::fw::service::cgiApp
{

	FwCgiAppResult::FwCgiAppResult(FwResponse* response)
	:
		response(response)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwCgiAppResult\033[0m" << endl;
		#endif
	}

	FwCgiAppResult::FwCgiAppResult(const FwCgiAppResult& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwCgiAppResult by copy\033[0m" << endl;
		#endif
	}

	FwCgiAppResult::~FwCgiAppResult()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwCgiAppResult\033[0m" << endl;
		#endif
	}

	FwResponse* FwCgiAppResult::getResponse()
	{
		return this->response;
	}

}