#pragma once

#include "eitri/fw/lib/service/FwServiceItf.h"
#include "eitri/fw/service/cgiApp/FwCgiAppInput.h"
#include "eitri/fw/service/cgiApp/FwCgiAppResult.h"

using eitri::fw::lib::service::FwServiceItf;
using eitri::fw::service::cgiApp::FwCgiAppInput;
using eitri::fw::service::cgiApp::FwCgiAppResult;

namespace eitri::fw::service::cgiApp
{

	class FwCgiAppItf
	:
		virtual public FwServiceItf<
			FwCgiAppInput,
			FwCgiAppResult
		>
	{

		public:

			virtual ~FwCgiAppItf() { };

	};

}