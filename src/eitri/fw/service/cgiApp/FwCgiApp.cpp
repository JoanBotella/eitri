#include "eitri/fw/lib/request/FwRequest.h"
#include "eitri/fw/lib/response/FwResponse.h"
#include "eitri/fw/lib/service/FwServiceOutput.h"
#include "eitri/fw/service/cgiApp/FwCgiApp.h"
#include "eitri/fw/service/cgiApp/FwCgiAppInput.h"
#include "eitri/fw/service/cgiApp/FwCgiAppResult.h"
#include "eitri/fw/service/router/FwRouterInput.h"
#include "eitri/fw/service/router/FwRouterItf.h"
#include "eitri/fw/service/router/FwRouterResult.h"
#include <iostream>
#include <string>

using eitri::fw::lib::request::FwRequest;
using eitri::fw::lib::response::FwResponse;
using eitri::fw::lib::service::FwServiceOutput;
using eitri::fw::service::cgiApp::FwCgiApp;
using eitri::fw::service::cgiApp::FwCgiAppInput;
using eitri::fw::service::cgiApp::FwCgiAppResult;
using eitri::fw::service::router::FwRouterItf;
using eitri::fw::service::router::FwRouterInput;
using eitri::fw::service::router::FwRouterResult;
using std::cout;
using std::endl;
using std::string;

namespace eitri::fw::service::cgiApp
{

	FwCgiApp::FwCgiApp(FwRouterItf* router)
	:
		router(router)
	{
		#ifdef debug
		cout << "\033[0;32mConstructing FwCgiApp\033[0m" << endl;
		#endif
	}

	FwCgiApp::FwCgiApp(const FwCgiApp& src)
	{
		#ifdef debug
		cout << "\033[0;33mConstructing FwCgiApp by copy\033[0m" << endl;
		#endif
	}

	FwCgiApp::~FwCgiApp()
	{
		#ifdef debug
		cout << "\033[0;31mDestructing FwCgiApp\033[0m" << endl;
		#endif
	}

	void FwCgiApp::runService()
	{
		cout << "FwCgiApp::runService" << endl;

		FwCgiAppInput* input = this->getInput();
		FwRequest* request = input->getRequest();
		FwRouterInput* routerInput = new FwRouterInput(request);

		FwServiceOutput<FwRouterResult>* routerOutput = this->router->run(routerInput);

		FwServiceOutput<FwCgiAppResult>* output = this->getOutput();

		if (routerOutput->hasErrors())
		{
			output->addErrors(
				routerOutput->getErrorsAfterHas()
			);
			this->setOutput(output);

			delete routerOutput;

			return;
		}

		if (!routerOutput->hasResult())
		{
			string* message = new string("The router had no errors nor result");
			FwError* err = new FwError(message);
			output->addError(err);
			this->setOutput(output);

			delete routerOutput;

			return;
		}

		FwRouterResult* routerResult = routerOutput->getResultAfterHas();
		FwResponse* response = routerResult->getResponse();
		FwCgiAppResult* result = new FwCgiAppResult(response);
		output->setResult(result);

		delete routerResult;
		delete routerOutput;

		this->setOutput(output);
	}

	void FwCgiApp::deleteInput()
	{
		FwCgiAppInput* input = this->getInput();
		delete input;
	}

}