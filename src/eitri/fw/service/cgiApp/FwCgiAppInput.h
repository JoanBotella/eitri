#pragma once

#include "eitri/fw/lib/request/FwRequest.h"

using eitri::fw::lib::request::FwRequest;

namespace eitri::fw::service::cgiApp
{

	class FwCgiAppInput
	{

		public:

			FwCgiAppInput(FwRequest* request);

			FwCgiAppInput(const FwCgiAppInput& src);

			~FwCgiAppInput();

			FwRequest* getRequest();

		private:

			FwRequest* request;

	};

}