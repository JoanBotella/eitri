#pragma once

#include "eitri/fw/lib/service/FwServiceAbs.h"
#include "eitri/fw/service/cgiApp/FwCgiAppInput.h"
#include "eitri/fw/service/cgiApp/FwCgiAppItf.h"
#include "eitri/fw/service/cgiApp/FwCgiAppResult.h"
#include "eitri/fw/service/router/FwRouterItf.h"

using eitri::fw::lib::service::FwServiceAbs;
using eitri::fw::service::cgiApp::FwCgiAppInput;
using eitri::fw::service::cgiApp::FwCgiAppItf;
using eitri::fw::service::cgiApp::FwCgiAppResult;
using eitri::fw::service::router::FwRouterItf;

namespace eitri::fw::service::cgiApp
{

	class FwCgiApp
	:
		virtual public FwCgiAppItf,
		public FwServiceAbs<
			FwCgiAppInput,
			FwCgiAppResult
		>
	{

		public:

			FwCgiApp(FwRouterItf* router);

			FwCgiApp(const FwCgiApp& src);

			~FwCgiApp();

		protected:

			void runService() override;

			void deleteInput() override;

		private:

			FwRouterItf* router;

	};

}