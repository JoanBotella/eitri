# Eitri

Web framework

## Project status

Early development stage. Absolutely not for production.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>